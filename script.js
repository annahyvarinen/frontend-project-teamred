

    const getData = async () => {
        const userkey = 'AJOAZ0qW'
        const response = await fetch('https://starttipakettiforumapi.azurewebsites.net/allMessages')
        const messages = await response.json()
        console.log(messages)

        messages.forEach(sender_name => {
            const pa = document.createElement('p')
            pa.innerHTML = sender_name.content + " -" + sender_name.sender_name
            document.querySelector('#all').appendChild(pa)
        })
        
        let userResults = messages.filter((message) => message['sender_key'] === userkey)
    console.log(userResults)
    
        userResults.forEach(userMessage => {
            console.log(userMessage)
            let id = userMessage.message_id
            // const divOwn = document.createElement("div")
            // divOwn.id = id
            const par = document.createElement('p')
            par.innerHTML = userMessage.content + " -" + userMessage.sender_name
            document.querySelector('#own').appendChild(par)

            const buttonEdit = document.createElement("button");
            buttonEdit.addEventListener("click", () => {
                console.log("this message's id is", id);
                window.location.href = "edit.html";
            });
        
            buttonEdit.innerHTML = "EDIT"
            document.querySelector("#own").appendChild(buttonEdit)

            const buttonDel = document.createElement("button")
            buttonDel.addEventListener("click", () => {
                console.log("delete")
                deleteData(id, messages)}
                // funktio joka lukee omat viestit jossa ei ole id:tä ja kirjottaa uuden listan owniin
                
        )
            buttonDel.innerHTML = "DELETE"
            document.querySelector("#own").appendChild(buttonDel)   
        
        })        

    }
    
    window.onload = getData
    
    
    const deleteData = async (id, messages) => {
        const userkey = 'AJOAZ0qW'
    const url = "https://starttipakettiforumapi.azurewebsites.net/deleteMessage/" + id
        const options = {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                userkey
            }
        }
        response = await fetch(url, options)
        location.reload()
    }


    

    // function newOwnList(messages) {
    //     userResults = messages.filter((message) => message['message_id'] !== id)
    //             console.log(userResults)
    //             const newPar = document.querySelector("#own")
    //             forEach
    // }
    
   
    

    document.addEventListener("nettisivu", function() {
        function loadWhiteBoxes() {
            const boxes = document.querySelectorAll('#whitecontainer > div');  //valitsee kaikki div-elementit, jotka ovat suoraan whitecontainerin alla. Eli valkoiset laatikot.
            const width = window.innerWidth;
            
            let count = 0;  //Montako laatikkoa näytetään sivuston pikselien perusteella
            if (width <= 600) {
                count = 2;
            } else if (width <= 1024) {
                count = 3;
            } else {
                count = 5;
            }
            
            boxes.forEach((box, index) => {  //Jos laatikon index on pienempi kuin count, laatikko näkyy. Muuten, laatikko piilotetaan
                if (index < count) {
                    box.style.display = 'flex';
                } else {
                    box.style.display = 'none';
                }
            });
        }
    
        window.addEventListener('resize', loadWhiteBoxes);//Aina kun selainikkunan kokoa muutetaan, suoritetaan loadWhiteBoxes-funktio uudelleen, jotta laatikoiden määrä päivittyy uuden ikkunan leveyden mukaisesti.
        loadWhiteBoxes();
    });


