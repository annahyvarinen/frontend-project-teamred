const getSingleData = async () => {
    const response = await fetch('https://starttipakettiforumapi.azurewebsites.net/allMessages/')
    const messages = await response.json()
    console.log(messages)

    const random = Math.floor(Math.random() * messages.length)
    const randomId = messages[random]
    console.log(messages[random])
    console.log(randomId)
    const id = randomId.message_id

    const response2 = await fetch('https://starttipakettiforumapi.azurewebsites.net/singleMessage/' + id)
    const messages2 = await response2.json()
    console.log(messages2)

    messages2.forEach(sender_name => {
        document.getElementById('getsingletext').innerHTML = sender_name.content
     
        document.querySelector('#getsinglewriter').innerHTML = "- " + sender_name.sender_name
  
    })
}

window.onload = getSingleData
